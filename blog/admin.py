from django.contrib import admin
from .models import Produit, Categorie
# Register your models here.

admin.site.register(Categorie)
admin.site.register(Produit)