from django.shortcuts import render

# Create your views here.
def index(request):
    posts =[
        {'id':1, 'title':'Iphone 6', 'detail':'Iphone 6 avec tous les caracteristiques et couleurs rouge, noir et rose'},
        {'id':2, 'title':'SamSung J5', 'detail':'SamSung J5 avec tous les caracteristiques et couleurs bleue, noir et rose'},
        {'id':3, 'title':'Nasco', 'detail':'Ecran plasma nasco LED'},
    ]
    return render(request,'blog/index.html',{'posts':posts})