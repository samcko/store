from django.db import models
from django.utils import timezone
# Create your models here.

# MODELE PRODUIT
class Produit(models.Model):
    title = models.CharField(max_length=255)
    mark = models.CharField(max_length=100)
    price = models.IntegerField()
    img= models.CharField(max_length=255)
    detail=models.TextField()
    date=models.DateTimeField(default=timezone.now, verbose_name="Date de parution")
    categorie = models.ForeignKey('Categorie', on_delete=models.CASCADE)
    
    class Meta:
        verbose_name = "produit"
        ordering = ['date']
    
    def __str__(self):
        """ 
        Cette méthode que nous définirons dans tous les modèles
        nous permettra de reconnaître facilement les différents objets que 
        nous traiterons plus tard dans l'administration
        """
        return self.title

# MODELE CATEGORIE
class Categorie(models.Model):
    nom = models.CharField(max_length=70)

    def __str__(self):
        return self.nom


# MODELE IMAGE
class Image(models.Model):
    img1 = models.CharField(max_length=255)
    img2 = models.CharField(max_length=255)
    img3 = models.CharField(max_length=255)
    produit = models.ForeignKey('Produit', on_delete=models.CASCADE)
    def __str__(self):
        return self.produit

# MODELE ENTREE
class Entree(models.Model):
    qte = models.IntegerField()
    date=models.DateTimeField(default=timezone.now, verbose_name="Date d'entrée")
    produit = models.ForeignKey('Produit', on_delete=models.CASCADE)
    def __str__(self):
        return self.produit

# MODELE CLIENT
class Client(models.Model):
    email = models.EmailField()
    date=models.DateTimeField(default=timezone.now, verbose_name="Date d'inscription'")
    contact = models.CharField(max_length=70)
    nom = models.CharField(max_length=255)
    mp = models.CharField(max_length=20)
    def __str__(self):
        return self.nom

# MODELE COMMANDER
class Commande(models.Model):
    qte = models.IntegerField()
    date=models.DateTimeField(default=timezone.now, verbose_name="Date de commande")
    produit = models.ForeignKey('Produit', on_delete=models.CASCADE)
    client = models.ForeignKey('Client', on_delete=models.CASCADE)
    def __str__(self):
        return self.client

# MODELE LIVRAISON OU SORTIE DU PRODUIT
class Sortie(models.Model):
    date=models.DateTimeField(default=timezone.now, verbose_name="Date de sortie")
    commande = models.ForeignKey('Commande', on_delete=models.CASCADE)
    def __str__(self):
        return self.commande

